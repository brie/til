# HOWTO Generate the GitLab GraphQL Schema

_(This assumes some familiarity with [feature development](https://docs.gitlab.com/ee/development/feature_development.html) for GitLab.)_

The GitLab GraphQL [schema](https://graphql.com/learn/schema/) can be **generated** with:

```
bundle exec rake gitlab:graphql:schema:dump
```

Read more in the docs on [generating the GraphQL schema](https://docs.gitlab.com/ee/development/fe_guide/graphql.html#generating-the-graphql-schema). 
