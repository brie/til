# SaaS Providers using AWS PrivateLink

AWS PrivateLink | Establish connectivity between VPCs and AWS services without exposing data to the internet

SaaS providers and their customers are one of the use cases for AWS PrivateLinks. The [Share your services through AWS PrivateLink](https://docs.aws.amazon.com/vpc/latest/privatelink/privatelink-share-your-services.html) page describes how to satisfy this use case. 

- Databricks: [Enable AWS PrivateLink](https://docs.databricks.com/en/administration-guide/cloud-configurations/aws/privatelink.html)
- JFrog: [Manage PrivateLink Connections](https://jfrog.com/help/r/jfrog-hosting-models-documentation/configure-cloud-security)
  - [DEMO – Actions speak louder than words](https://jfrog.com/user-conference/demo-actions-speak-louder-than-words/)
  - [PRIVATLINK: How To Establish Secure Connection With SaaS Artifactory Using AWS Privat Link](https://jfrog.com/help/r/privatlink-how-to-establish-secure-connection-with-saas-artifactory-using-aws-privat-link)
- Snowflake: [Setup Considerations When Integrating AWS PrivateLink With Snowflake](https://community.snowflake.com/s/article/Setup-Considerations-When-Integrating-AWS-PrivateLink-With-Snowflake)

The release history for AWS PrivateLink dates back to [May 11, 2015](https://docs.aws.amazon.com/vpc/latest/privatelink/doc-history.html). At that time, we only had VPC Endpoints. AWS PrivateLink was [announced](https://aws.amazon.com/blogs/aws/new-aws-privatelink-endpoints-kinesis-ec2-systems-manager-and-elb-apis-in-your-vpc/) on November 08, 2017 and billed as the "newest generation of VPC Endpoints".   

## 📚 READmore

- [AWS PrivateLink Partners](https://aws.amazon.com/privatelink/partners/)
- AWS: [AWS services that integrate with AWS PrivateLink](https://docs.aws.amazon.com/vpc/latest/privatelink/aws-services-privatelink-support.html)
- [AWS PrivateLink, Explained](https://www.megaport.com/blog/aws-privatelink-explained/)
- [New – AWS PrivateLink for AWS Services: Kinesis, Service Catalog, EC2 Systems Manager, Amazon EC2 APIs, and ELB APIs in your VPC](https://aws.amazon.com/blogs/aws/new-aws-privatelink-endpoints-kinesis-ec2-systems-manager-and-elb-apis-in-your-vpc/)
- [AWS PrivateLink FAQs](https://aws.amazon.com/privatelink/faqs/)
- [How do I set up end-to-end HTTPS connectivity with AWS PrivateLink?](https://repost.aws/knowledge-center/privatelink-https-connectivity)
- [Enhanced Network Security with AWS PrivateLink and MongoDB Atlas](https://www.mongodb.com/blog/post/enhanced-security-measures-in-atlas-with-aws-privatelink)
- [AWS PrivateLink, Explained](https://www.megaport.com/blog/aws-privatelink-explained/)
- Taccoform: [AWS PrivateLink Part 1](https://www.taccoform.com/posts/aws_pvt_link_1/)
- [AWS PrivateLink Reference Architectures](https://github.com/aws-samples/private-saas-with-aws-privatelink)
