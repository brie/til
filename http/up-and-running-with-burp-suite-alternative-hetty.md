# Up and running with Burp Suite alternative: Hetty

> [Hetty is an HTTP toolkit for security research.](https://github.com/dstotijn/hetty)

Use a port other than the default (`8080`):

```
hetty --addr :8088
```

## Use the proxy

In order to get request and response interception via the proxy built into `hetty`, you should pass `--chrome`. Building on our example from earlier:

```
hetty --chrome --addr :8088
```

[docs](https://hetty.xyz/docs/getting-started/#use-the-proxy)


When intercepting responses, consider a filter like:

```
statusCode =~ "^5"
```

## 📚 READmore

- [Source](https://github.com/dstotijn/hetty)
- [Intercepting HTTP traffic](https://hetty.xyz/docs/guides/intercept)
