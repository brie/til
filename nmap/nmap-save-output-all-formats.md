# Save nmap output in all available formats

```
nmap -oA whatever
```

```
# nmap -p 443 -oA whatever scanme.nmap.org
Starting Nmap 7.95 ( https://nmap.org ) at 2024-11-13 22:54 EST
Nmap scan report for scanme.nmap.org (45.33.32.156)
Host is up (0.058s latency).

PORT    STATE  SERVICE
443/tcp closed https

Nmap done: 1 IP address (1 host up) scanned in 0.15 seconds
```

```
# ls whatever*
whatever.gnmap whatever.nmap  whatever.xml
```

## READmore

- [Chapter 13. Nmap Output Formats](https://nmap.org/book/output.html)
- [Saving output in all formats](https://www.oreilly.com/library/view/nmap-network-exploration/9781786467454/3d5626cd-48ab-44e4-a0b9-3d349862f814.xhtml)
